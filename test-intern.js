class MyBigNumber {
  static sum(num1, num2) {
    let result = "";
    let carry = 0; 

 
    for (let i = num1.length - 1, j = num2.length - 1;i >= 0 || j >= 0 || carry > 0; i--, j--) {
      const digit1 = i >= 0 ? parseInt(num1[i]) : 0; 
      const digit2 = j >= 0 ? parseInt(num2[j]) : 0; 
      const sum = digit1 + digit2 + carry; 
      result = (sum % 10) + result; 
      carry = Math.floor(sum / 10); 
      console.log('digit1, digit2, sum, carry, result ', digit1, digit2, sum, carry, result )
    }

    return result;
  }
}

const num1 = "1234";
const num2 = "897";
console.log("Sum:", MyBigNumber.sum(num1, num2)); 
